package sheridan;

/**
*
* @author Seby
*
*/

public class Celsius {

	
	public static int fromFahrenheit(int a) {
		//I know that I could have just a return statement, but I like to see the steps of my code sometimes
		double result = Math.ceil(((a-32.0) * 5.0/9.0));		
		return (int)result;
	}
}
