package sheridan;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.Test;

/**
 * 
 * @author Seby
 *
 */

public class CelsiusTest {

	@Test
	public void testFromFahrenheitRegular() {
		int celsius = Celsius.fromFahrenheit(90);
		
		assertTrue("Invalid Celsius", celsius == 33);
	}
	
	@Test
	public void testFromFahrenheitException() {
		int celsius = Celsius.fromFahrenheit(90);
		assertFalse("Invalid Celsius", celsius == 32.2222);
	}
	
	@Test
	public void testFromFahrenheitBoundaryIn() {
		int celsius = Celsius.fromFahrenheit(90);
		assertTrue("Invalid Celsius", celsius == 33.0);
	}
	
	@Test
	public void testFromFahrenheitBoundaryOut() {
		int celsius = Celsius.fromFahrenheit(90);
		assertFalse("Invalid Celsius", celsius == 33.00001);
	}

}
